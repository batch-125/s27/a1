let http = require("http")

const PORT = 3000;

http.createServer((request, response) => {
	if (request.url == "/login"){
		response.writeHead( 200,
			{"Content-Type": "text/html"});
		response.end("Welcome to Login Page");
	} else {
		response.writeHead( 404,
			{"Content-Type": "text/html"});
		response.end("ERROR 404: I am sorry. The page you are looking for cannot be found.");
	}
}).listen(PORT);

console.log("Server is Successfully Running")