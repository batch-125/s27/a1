// (1) What directive is used by Node.js in loading the modules it needs?

require  
require("http")

// (2) What Node.js module contains a method for server creation?

HTTP

// (3) What is the method of the http object responsible for a creating a server using Node.js?

createServer()

// (4) What method of the response object allows us to set status codes and content types?
res.writeHead()

// (5) Where will the console.log() output its contents when run in Node.js

It will run in git bash terminal

// (6) What property of the request object contains the address' endpoint

url
req.url